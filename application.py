from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QMessageBox
import linsimpy

import Espaco_Tuplas as ts
lista_nuvens = []

users = []
environments = []
devices = []

tse = linsimpy.TupleSpaceEnvironment()

def atualizaTela():
    list_devices = ts.listDevices()
    list_usuarios = ts.listUsers()
    openSegundaTela(list_devices,list_usuarios)
    primeira_tela.lineEdit.clear()
    primeira_tela.lineEdit_2.clear()
    primeira_tela.lineEdit_3.clear()

def cadastraContainer():
    env    = primeira_tela.lineEdit.text()
    device = primeira_tela.lineEdit_2.text()
    user   = primeira_tela.lineEdit_3.text()
    
    data = [env, device, user]
    incrementaAmbimente = 0
    if(data[0]!= ''):
        envName = data[0]
        if(ts.existe('ENV', envName) == 0):
            ts.criaAmbiente(envName)
        else:
            QMessageBox.about(primeira_tela,"ALERTA", "AMBIENTE JÁ EXISTE")

    if(data[1]!= ''):
        deviceName = data[1]
        if(ts.existe('DEVICE', deviceName) == 0):
            ts.criaDispositivo(deviceName, envName)
        else:
            QMessageBox.about(primeira_tela,"ALERTA", "DISPOSITIVO JÁ EXISTE")

    if(data[2]!= ''):
        nomeUser = data[2]
        if(ts.existe("USER",nomeUser) == 0):
            ts.criaUsuario(nomeUser, envName)
        else:
            QMessageBox.about(primeira_tela,"ALERTA", "USER JÁ EXISTE") 

    atualizaTela()

def openSegundaTela(devices, users):
    segunda_tela.show()
    segunda_tela.listWidget.clear()
    segunda_tela.listWidget_3.clear()

    if(len(devices) > 0):
        for n in devices:
            segunda_tela.listWidget.addItem(n)
            segunda_tela.label_6.setText(str(len(devices)))
    if(len(users) > 0):            
        for h in users:
            segunda_tela.listWidget_3.addItem(h)
            segunda_tela.label_7.setText(str(len(users)))

def openQuartaTela():
    quarta_tela.show()
    lista_users = ts.listUsersChat()
    quarta_tela.comboBox_2.addItems(lista_users)
    quarta_tela.comboBox_3.addItems(lista_users)
   
def chat():    
    p1 = quarta_tela.comboBox_2.currentText()
    p2 = quarta_tela.comboBox_3.currentText()
    verificacaoAmbientes = verifyAmbiente(p1, p2)
    if verificacaoAmbientes == 0:
        msg = quarta_tela.lineEdit.text()
        ts.enviamsg(p1,p2,msg)
        resp = ts.recebeMsg(p1,p2,msg)
        quarta_tela.listWidget.addItem(f"{str(resp[1])} recebendo msg de {str(resp[0])}: {str(resp[2])}")
        quarta_tela.lineEdit.clear()
    elif verificacaoAmbientes == 1:
        QMessageBox.about(quarta_tela,"ALERTA", "O USUÁRIO NÃO ESTÁ NO MESMO AMBIENTE")
    elif verificacaoAmbientes == 2:
        QMessageBox.about(quarta_tela,"ALERTA", "O USUÁRIO NÃO PODE ENVIAR MENSAGEM PARA SI PRÓPRIO")


def verifyAmbiente(p1, p2):
    if p1 == p2: return 2
    ambiente1 = p1.split(' - ')[1]
    ambiente2 = p2.split(' - ')[1]
    if (ambiente1 != ambiente2): return 1
    else: return 0
  

###########    MAIN   ###############
app=QtWidgets.QApplication([])
primeira_tela=uic.loadUi("primeira_tela.ui")
primeira_tela.show()
segunda_tela=uic.loadUi("segunda_tela.ui")
quarta_tela=uic.loadUi("quarta_tela.ui")
primeira_tela.pushButton.clicked.connect(cadastraContainer)
segunda_tela.pushButton.clicked.connect(openQuartaTela)
quarta_tela.pushButton.clicked.connect(chat)

app.exec()
