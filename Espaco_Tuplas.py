import linsimpy
from tkinter import *

tse = linsimpy.TupleSpaceEnvironment()

arrayEnvNames = []
arrayTupleDevices = []
arrayTupleUsers   = []

#inp = Lê e remove atomicamente – consome – uma tupla, gerando KeyError se não for encontrado
#out = Retorna um evento simple que escreve uma tupla
#rdp = Lê uma tupla de forma não destrutiva, gerando KeyError se não for encontrado.
def criaAmbiente(nomeAmb):
    nomeAmb = str(nomeAmb)
    tse.out(("ENV", nomeAmb))

    tse.out((nomeAmb, tuple([])))
    tse.out((nomeAmb + 'u', tuple([])))
    arrayEnvNames.append(nomeAmb)
    print(f"Ambiente: {nomeAmb} criado")

def criaDispositivo(deviceName, envName):
    tse.out(("DEVICE", deviceName))

    envDevices = tse.inp((envName, object))
    envDevices = list(envDevices[1])
    envDevices.append(deviceName)
    tse.out((envName, tuple(envDevices)))

    print(f"Dispositivo: {deviceName} criado")

def criaUsuario(userName, envName): 
    tse.out(("USER", userName))
    envName = str(envName) + 'u'
    envUsers = tse.inp((envName, object))
    envUsers = list(envUsers[1])
    envUsers.append(userName)
    tse.out((envName, tuple(envUsers)))

    print(f"Usuário: {userName} criado")

# def deleteItem(tipo_tupla,nome):
#     try:
#         tupla = tse.inp((tipo_tupla,nome))
#         return 1
#     except:
#         return 0

# def deleteTuple(tipo_tupla, nome):
#     try:
#         tupla = tse.inp((tipo_tupla,object))
#         nova_tupla = list(tupla[1])
#         nova_tupla.remove(nome)
#         tse.out((tipo_tupla,tuple(nova_tupla)))
#         return(f"Tupla {nome} excluida")
#     except:
#         return(f"Tupla {nome} não encontrada")

def existe(tipoTupla, nome):
    try:
        tupla = tse.rdp((tipoTupla, nome))
        print(f"TUPLA {tupla[1]} JÁ EXISTE")
        return 1
    except:
        return 0

            
def listDevices():
    arrayListDevices = []
    for ambiente in arrayEnvNames:
        devices = tse.rdp((ambiente, object))
        devices = list(devices[1])
        for device in devices:
            arrayListDevices.append(str(device) + ' (AMBIENTE: ' + str(ambiente) + ')')

    return arrayListDevices

def listUsers():
    arrayListUsers = []
    for ambiente in arrayEnvNames:
        ambienteReal = ambiente
        ambiente = str(ambiente) + 'u'
        users = tse.rdp((ambiente, object))
        users = list(users[1])
        for user in users:
            arrayListUsers.append(str(user) + ' (AMBIENTE: ' + str(ambienteReal) + ')')

    return arrayListUsers

def listUsersChat():
    arrayListUsers = []
    for ambiente in arrayEnvNames:
        ambienteReal = ambiente
        ambiente = str(ambiente) + 'u'
        users = tse.rdp((ambiente, object))
        users = list(users[1])
        for user in users:
            arrayListUsers.append(str(user) + ' - ' + str(ambienteReal))

    return arrayListUsers

def enviamsg(p1,p2,msg):
    tse.out((p1,p2,msg))

def recebeMsg(p1,p2,msg):
    mensagem = tse.inp((p1,p2,msg))
    return (mensagem)

